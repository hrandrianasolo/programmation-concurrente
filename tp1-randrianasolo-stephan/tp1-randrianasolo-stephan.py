

# Author : Stephan RANDRIANASOLO
# N°     : 15607284
# Gitlab : https://gitlab.com/hrandrianasolo/programmation-concurrente

from math import sqrt


def is_positiv(cote1: int,
               cote2: int,
               cote3: int) -> bool:
    if cote1 < 0 or cote2 < 0 or cote3 < 0:
        return False
    else:
        return True

    return False


def definit_triangle(cote1: int,
                     cote2: int,
                     cote3: int) -> bool:
    if not is_positiv(cote1, cote2, cote3):
        return False
    else:
        somme_des_cote = cote1 + cote2 + cote3
        if somme_des_cote >= 2 * max([cote1, cote2, cote3]):
            return True
        else:
            return False
    return False


def isocele(cote1: int,
            cote2: int,
            cote3: int) -> bool:
    if definit_triangle(cote1, cote2, cote3):
        if cote1 == cote2 or cote1 == cote3:
            return True
        elif cote2 == cote3:
            return True
    return False


def aire_ordonne(cote1: int,
                 cote2: int,
                 cote3: int) -> float:
    result = 0.0

    if is_positiv(cote1, cote2, cote3):
        u1 = min(cote1, cote2, cote3)
        u3 = max(cote1, cote2, cote3)
        u2 = (cote1 + cote2 + cote3) - u1 - u3

        result = (((u1 ** 2) - (u2 ** 2) + (u3 ** 2)) / 2) ** 2
        result = (u1 ** 2) * (u3 ** 2) - result
        result = 0.5 * sqrt(result)

    return result


def nb_triangles_speciaux(param1: int,
                          param2: int) -> int:
    """
        pour cette dernière exercice je ne sais pas
        comment gerer comment savoir si le triangle
        a déjà était tester sans utilisation
        d'une liste
    """
    result = 0
    for cote1 in range(param1, param2):
        for cote2 in range(param1, param2):
            for cote3 in range(param1, param2):
                if definit_triangle(cote1, cote2, cote3):
                    if aire_ordonne(cote1, cote2, cote3) == float(cote1 + cote2 + cote3):
                        result += 1
    return result


if __name__ == "__main__":
    print("\n")
    print("isocele(4, 3, 2) = " + str(isocele(4, 3, 2)))
    print("isocele(4, 3, 3) = " + str(isocele(4, 3, 3)))
    print("isocele(4, 4, 4) = " + str(isocele(4, 4, 4)))
    print("isocele(1, 1, 20) = " + str(isocele(1, 1, 20)))
    print("\n")
    print("aire_ordonne(4, 2, 3) = " + str(aire_ordonne(4, 2, 3)))
    print("aire_ordonne(4, 3, 3) = " + str(aire_ordonne(4, 3, 3)))
    print("aire_ordonne(4, 4, 4) = " + str(aire_ordonne(4, 4, 4)))
    print("aire_ordonne(3, 4, 5) = " + str(aire_ordonne(4, 3, 5)))
    print("aire_ordonne(13,14,15) = " + str(aire_ordonne(13, 14, 15)))
    print("aire_ordonne(1, 1, 1) = " + str(aire_ordonne(1, 1, 1)))
    print("\n")
    print("definit_triangle(1, 1, 20) = " + str(definit_triangle(1, 1, 20)))
    print("definit_triangle(4, 2, 3) = " + str(definit_triangle(4, 2, 3)))
    print("definit_triangle(4, 4, 4) = " + str(definit_triangle(4, 4, 4)))
    print("\n")
    print("nb_triangles_speciaux(1, 20) = " + str(nb_triangles_speciaux(1, 20)))
