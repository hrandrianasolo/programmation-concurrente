from threading import Thread
from multiprocessing import Process


def calcul_square(list_nombres):
    for x in list_nombres:
        print(str(x ** 2) + ' ,')


def calcul_cube(list_nombres):
    for x in list_nombres:
        print(str(x ** 3) + ' ,')


if __name__ == '__main__':
    threads = []
    process = []

    list_nombres = [2, 3, 8, 9, 12]

    print("----- Begin of Thread -----")
    threads.append(Thread(target=calcul_square, args=(list_nombres,)))
    threads.append(Thread(target=calcul_cube, args=(list_nombres,)))

    for tmp in threads:
        tmp.start()

    for tmp in threads:
        tmp.join()

    print("----- End of Thread -----")

    print("----- Begin of Process -----")
    process.append(Process(target=calcul_square, args=(list_nombres,)))
    process.append(Process(target=calcul_cube, args=(list_nombres,)))

    for tmp in process:
        tmp.start()

    for tmp in process:
        tmp.join()

    print("----- End of Process -----")

