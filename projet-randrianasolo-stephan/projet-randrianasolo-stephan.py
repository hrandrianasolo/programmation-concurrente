from threading import Thread, Lock
from collections import deque
import time
import random
import sys


class Producer(Thread):

    def __init__(self, kwargs=None):
        super(Producer, self).__init__()
        self.lock = kwargs['lock']
        self.shared_data = kwargs['shared_data']
        self.thread_id = kwargs['thread_id']
        self.is_stopped = False

        return

    def produce_item(self):
        data = random.randrange(1, 100)
        self.shared_data.append(data)
        print("Producer id {} : produce {} result {}".format(self.thread_id,
                                                             data,
                                                             self.shared_data))

    @staticmethod
    def wait():
        time.sleep(random.uniform(0.5, 1.0))

    def stop(self):
        self.is_stopped = True

    def run(self):

        while not self.is_stopped:
            if not self.lock.locked():
                with self.lock:
                    self.produce_item()
                self.wait()


class Consumer(Thread):

    def __init__(self, kwargs=None):
        super(Consumer, self).__init__()
        self.lock = kwargs['lock']
        self.shared_data = kwargs['shared_data']
        self.thread_id = kwargs['thread_id']
        self.is_stopped = False

        return

    def consum_item(self):

        if self.shared_data:
            print("Consumer id {} : consume {} result {}".format(self.thread_id,
                                                                 self.shared_data.popleft(),
                                                                 self.shared_data))

    @staticmethod
    def wait():
        time.sleep(random.uniform(0.5, 1.0))

    def stop(self):
        self.is_stopped = True

    def run(self):

        while not self.is_stopped:
            if self.shared_data:
                self.consum_item()
                self.wait()


def main(nbr_producer, nbr_consumer, nbr_sec_prog_alive):
    lock = Lock()
    items = deque()
    producers = []
    consumers = []
    is_time_elapsed = False

    for producer_id in range(0, nbr_producer):
        producers.append(Producer(kwargs={'lock': lock,
                                          'shared_data': items,
                                          'thread_id': producer_id}))

    for consumer_id in range(0, nbr_consumer):
        consumers.append(Consumer(kwargs={'lock': lock,
                                          'shared_data': items,
                                          'thread_id': consumer_id}))

    threads = producers + consumers
    random.shuffle(threads)

    time_start = time.perf_counter()
    for thread in threads:
        thread.start()

    while not is_time_elapsed:
        if time.perf_counter() - time_start > nbr_sec_prog_alive:
            for thread in threads:
                thread.stop()
            is_time_elapsed = True


if __name__ == '__main__':
    if len(sys.argv) < 4:
        print("Usage : python projet-randrianasolo-stephan.py [NBR_PRODUCER] [NBR_CONSUMER] [NBR_SEC_PROG_ALIVE]")
        exit(1)

    try:
        int(sys.argv[1])
    except ValueError:
        print("[NBR_PRODUCER] must be an Integer.")
        exit(1)
    try:
        int(sys.argv[2])
    except ValueError:
        print("[NBR_CONSUMER] must be an Integer.")
        exit(1)
    try:
        float(sys.argv[3])
    except ValueError:
        print("[NBR_SEC_PROG_ALIVE] must be an Float.")
        exit(1)

    main(int(sys.argv[1]), int(sys.argv[2]), float(sys.argv[3]))
