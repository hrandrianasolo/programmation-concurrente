
# Author : RANDRIANASOLO Stephan
# N°     : 15607284

"""
    Comme je suis déjà un peu avancée en python,
    je mettrais que les fonctions ou remarque que
    j'ai pu apprendre de nouveaux ici
"""


def ask_ok(prompt, retries=4, reminder='Please try again!'):
    """
    :param variable = default value
    """
    while True:
        ok = input(prompt)
        if ok in ('y', 'ye', 'yes'):
            return True
        if ok in ('n', 'no', 'nop', 'nope'):
            return False
        retries = retries - 1
        if retries < 0:
            raise ValueError('invalid user response')
        print(reminder)


def parrot(voltage, state='a stiff', action='voom', type='Norwegian Blue'):
    """
    :param voltage:obligatoire à l'appel de la fonction car les autres ont
                   déjà une valeur par defaut

    :var     d = {"voltage": "four million", "state": "bleedin' demised", "action": "VOOM"}
    :call    parrot(**d)
    :output  This parrot wouldn't VOOM if you put four million volts through it. E's bleedin' demised !
    """
    print("-- This parrot wouldn't", action, end=' ')
    print("if you put", voltage, "volts through it.")
    print("-- Lovely plumage, the", type)
    print("-- It's", state, "!")


def cheeseshop(kind, *arguments, **keywords):
    """
    :param arguments: tous les arguments sans clef
    :param keywords: tous les arguments avec clef
    :return:
    """
    print("-- Do you have any", kind, "?")
    print("-- I'm sorry, we're all out of", kind)
    for arg in arguments:
        print(arg)
    print("-" * 40)
    keys = sorted(keywords.keys())
    for kw in keys:
        print(kw, ":", keywords[kw])


def concat(*args, sep="/"):
    return sep.join(args)


def create_table():
    # Deux façon de créer une table
    for x in range(1, 11):
        print(repr(x).rjust(2), repr(x * x).rjust(3), end=' ')
        # Note use of 'end' on previous line
        print(repr(x * x * x).rjust(4))

    for x in range(1, 11):
        print('{0:2d} {1:3d} {2:4d}'.format(x, x * x, x * x * x))


def divide(x, y):
    try:
        result = x / y
    except ZeroDivisionError:
        print("division by zero!")
    else:
        print("result is", result)
    finally:
        print("executing finally clause")


class Mapping:
    def __init__(self, iterable):
        self.items_list = []
        self.__update(iterable)

    def update(self, iterable):
        for item in iterable:
            self.items_list.append(item)

    __update = update   # private copy of original update() method


class MappingSubclass(Mapping):

    def update(self, keys, values):
        # provides new signature for update()
        # but does not break __init__()
        for item in zip(keys, values):
            self.items_list.append(item)


def commentaire():
    pass
    """ 
    
    FIFO
    from collections import deque
    queue = deque(["Eric", "John", "Michael"])
    queue.append("Terry")           # Terry arrives
    queue.append("Graham")          # Graham arrives
    queue.popleft()                 # The first to arrive now leaves
    out: 'Eric'
    queue.popleft()                 # The second to arrive now leaves
    out: 'John'
    queue                           # Remaining queue in order of arrival
    out: deque(['Michael', 'Terry', 'Graham'])

    squares = [x**2 for x in range(10)]
    
    #création d'une dict à partir d'une list paires clé-valeur stockées sous la forme de tuples
    dict([('sape', 4139), ('guido', 4127), ('jack', 4098)])
    dict(sape=4139, guido=4127, jack=4098)
    
    str.zfill(5) comble une String à gauche avec des zéros
    out:
    
    '!a' (appliquer ascii()), '!s' (appliquer str()) et '!r' (appliquer repr())
    print('My hovercraft is full of {!r}.'.format(contents))
    """
