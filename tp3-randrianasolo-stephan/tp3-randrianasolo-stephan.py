
# Author : RANDRIANASOLO Stephan
# N°     : 15607284

import time
import sys


def compte_mots(phrase: str) -> int:
    """
    Exercice 1
    """
    resultat = 0

    resultat = len(' '.join(phrase.split()).strip().split(' '))

    return resultat


def remplace_multiple(mot1: str, mot2: str, n_pos: int) -> str:
    """
    Exercice 2
    """
    if len(mot1) == 0:
        return mot2
    if len(mot2) == 0:
        return mot1

    resultat = mot1[:n_pos] + mot2[0]
    tmp_mot1 = mot1[n_pos:]
    tmp_mot2 = mot2[1:]

    while len(tmp_mot2) != 0:
        resultat = resultat + tmp_mot1[1:n_pos] + tmp_mot2[0]
        tmp_mot1 = tmp_mot1[n_pos:]
        tmp_mot2 = tmp_mot2[1:]

    if len(tmp_mot1) != 0 and len(tmp_mot2) == 0:
        resultat = resultat + tmp_mot1

    if len(tmp_mot1) == 0 and len(tmp_mot2) != 0:
        resultat = resultat + tmp_mot1

    return resultat


def terme_u(valeur: int) -> int:
    """
    Exercice 3 - énoncé 1
    """
    if valeur == 0:
        return 1

    elif valeur >= 1:
        return terme_u(valeur-1) * (2**valeur) + valeur


def serie(valeur):
    """
    Exercice 3 - énoncé 2
    """
    resultat = 0

    for i in range(0, valeur+1):
        resultat += terme_u(i)

    return resultat


def serie_v2(valeur):
    """
    Exercice 3 - énoncé 3
    """
    resultat = 0
    dict_tmp = {}
    dict_tmp['0'] = 1

    for i in range(0, valeur+1):
        if str(i) in dict_tmp.keys():
            continue
        else:
            dict_tmp[str(i)] = dict_tmp[str(i-1)] * (2**i) + i

    for key, value in dict_tmp.items():
        resultat += value

    return resultat


def factorielle(valeur):
    resultat = 1

    if valeur == 0:
        resultat = 1
    else:
        for i in range(1, valeur+1):
            resultat *= i

    return resultat


if __name__ == "__main__":

    print("Erxercie 1" )
    print("compte_mots('') : " + str(compte_mots("")))
    print("compte_mots('il ingurgite impunément un iguane') : " +
          str(compte_mots("il ingurgite impunément un iguane")))
    print("compte_mots('coursdeprogrammation') : " + str(compte_mots("coursdeprogrammation")))
    print("compte_mots(' Attention aux    espaces consécutifs ou terminaux ') : " +
          str(compte_mots(" Attention aux    espaces consécutifs ou terminaux ")))
    print("")
    print("Erxercie 2")
    print("remplace_multiple('','',2) : " + str(remplace_multiple("", "", 2)))
    print("remplace_multiple('abacus','oiseau',2) : " + str(remplace_multiple("abacus", "oiseau", 2)))
    print("remplace_multiple('hirondelles','nid',3) : " + str(remplace_multiple("hirondelles", "nid", 3)))

    print("Erxercie 3 - énoncé 1")
    print("terme_u(0) : " + str(terme_u(0)))
    print("terme_u(1) : " + str(terme_u(1)))
    print("terme_u(5) : " + str(terme_u(5)))
    print("terme_u(10) : " + str(terme_u(10)))
    print("")
    print("Erxercie 3 - énoncé 2")
    print("serie(0) : " + str(serie(0)))
    print("serie(1) : " + str(serie(1)))
    print("serie(5) : " + str(serie(5)))
    print("serie(10) : " + str(serie(10)))
    print("")
    print("Erxercie 3 - énoncé 3")
    print("serie_v2(0) : " + str(serie_v2(0)))
    print("serie_v2(1) : " + str(serie_v2(1)))
    print("serie_v2(5) : " + str(serie_v2(5)))
    print("serie_v2(10) : " + str(serie_v2(10)))

    print("")
    print("Erxercie 4")
    print("factorielle(1) : " + str(factorielle(1)))
    print("factorielle(2) : " + str(factorielle(2)))
    print("factorielle(3) : " + str(factorielle(3)))
    print("factorielle(4) : " + str(factorielle(4)))
